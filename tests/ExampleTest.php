<?php
/**
 * Project: Push Notifications: Channels Routing Resolver
 * Author:  planet17
 */
namespace Planet17\PushNotifications\ChannelsRoutingResolver;

use PHPUnit\Framework\TestCase;


/**
 * Class ExampleTest - Test not for class, but for example of works.
 *
 * @package Planet17\PushNotifications\ChannelsRoutingResolver
 */
class ExampleTest extends TestCase
{
    public function testSimple()
    {
        $resolver = new Resolver(new Map(new Rule()));
        $current = [Rule::OPT_NAME_PLATFORM => 'ios', Rule::OPT_NAME_ENVIRONMENT => 'test'];
        $this->assertSame('iosTest', $resolver->resolve($current));
    }
}
