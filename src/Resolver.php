<?php
/**
 * Project: Push Notifications: Channels Routing Resolver
 * Author:  planet17
 */

namespace Planet17\PushNotifications\ChannelsRoutingResolver;


use Planet17\RulesMapResolver\Resolvers\Simple;


/**
 * Class Resolver - Example of default class Resolver for use.
 *
 * @package Planet17\PushNotifications\ChannelsRoutingResolver
 */
class Resolver extends Simple
{
}
