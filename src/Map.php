<?php
/**
 * Project: Push Notifications: Channels Routing Resolver
 * Author:  planet17
 */

namespace Planet17\PushNotifications\ChannelsRoutingResolver;

use Planet17\RulesMapResolver\Map as BaseMap;

/**
 * Class Map - Example of default class Map for use.
 *
 * @method Rule getPrototype()
 *
 * @package Planet17\PushNotifications\ChannelsRoutingResolver
 */
class Map extends BaseMap
{
    public function setUp()
    {
        $this->addRule(
            'desktop',
            $this->getPrototype()
                 ->addPlatform('web')
                 ->addPlatform('webDesktop')
                 ->addPlatform('desktop')
                 ->addEnvironment('production')
        );

        $this->addRule(
            'mobile',
            $this->getPrototype()
                 ->addPlatform('mobile')
                 ->addPlatform('mobileSites')
                 ->addPlatform('webMobile')
                 ->addEnvironment('production')
        );

        $this->addRule(
            'android',
            $this->getPrototype()
                 ->addPlatform('android')
                 ->addEnvironment('production')
        );

        $this->addRule(
            'ios',
            $this->getPrototype()
                 ->addPlatform('ios')
                 ->addPlatform('apple')
                 ->addEnvironment('production')
        );

        $this->addRule(
            'desktopTest',
            $this->getPrototype()
                 ->addPlatform('web')
                 ->addPlatform('webDesktop')
                 ->addPlatform('desktop')
                 ->addEnvironment('development')
                 ->addEnvironment('test')
        );

        $this->addRule(
            'mobileTest',
            $this->getPrototype()
                 ->addPlatform('mobile')
                 ->addPlatform('mobileSites')
                 ->addPlatform('webMobile')
                 ->addEnvironment('development')
                 ->addEnvironment('test')
        );

        $this->addRule(
            'androidTest',
            $this->getPrototype()
                 ->addPlatform('android')
                 ->addEnvironment('development')
                 ->addEnvironment('test')
        );

        $this->addRule(
            'iosTest',
            $this->getPrototype()
                 ->addPlatform('ios')
                 ->addPlatform('apple')
                 ->addEnvironment('development')
                 ->addEnvironment('test')
        );
    }
}
