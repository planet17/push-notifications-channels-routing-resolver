<?php
/**
 * Project: Push Notifications: Channels Routing Resolver
 * Author:  planet17
 */

namespace Planet17\PushNotifications\ChannelsRoutingResolver;


use Planet17\RulesMapResolver\Contracts\RuleContract;
use Planet17\RulesMapResolver\Rule as BaseRule;


/**
 * Class PlatformRule - Example of default class Rule for use.
 *
 * Class implements simple Rule what can be matching by two options:
 *
 * ```
 * 1) Platform of receiver
 * 2) Environment name where script will works.
 *
 * @package Planet17\PushNotifications\ChannelsRoutingResolver
 */
class Rule extends BaseRule
{
    /**
     * Platform of Receivers.
     *
     * @const OPT_NAME_PLATFORM
     */
    const OPT_NAME_PLATFORM = 'platform';

    /**
     * Current environment where script will works.
     *
     * @const OPT_NAME_ENVIRONMENT
     */
    const OPT_NAME_ENVIRONMENT = 'environment';


    /** @inheritdoc */
    public function getOptsNames():array
    {
        return [self::OPT_NAME_PLATFORM, self::OPT_NAME_ENVIRONMENT];
    }


    /** @inheritdoc */
    public function getRequiredOptsNames():array
    {
        return [];
    }


    /**
     * Method more secure append one of `Option` values.
     *
     * @param string $value
     *
     * @return RuleContract|Rule
     */
    public function addPlatform(string $value):RuleContract
    {
        return $this->addOptValue(self::OPT_NAME_PLATFORM, $value);
    }


    /**
     * Method more secure append one of `Option` values.
     *
     * @param string $value
     *
     * @return RuleContract|Rule
     */
    public function addEnvironment(string $value):RuleContract
    {
        return $this->addOptValue(self::OPT_NAME_ENVIRONMENT, $value);
    }
}
